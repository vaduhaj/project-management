package com.PM;

import com.PM.Calendar.CalendarList;
import com.PM.EditingCells.DateEditingCell;
import com.PM.EditingCells.TextEditingCell;
import com.PM.ProjectStructure.ProjectDTO;
import com.PM.ProjectStructure.ProjectDataAction;
import com.PM.ProjectStructure.ProjectTask;
import com.PM.ProjectStructure.ProjectData;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Duration;
import org.controlsfx.dialog.ExceptionDialog;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

public class TreeTableProjectTask {

    public final static String dafaultStyle = "-fx-padding: 10;" +
            "-fx-border-style: solid inside;" +
            "-fx-border-width: 2;" +
            "-fx-border-insets: 5;" +
            "-fx-border-radius: 5;";
    private static final DataFormat SERIALIZED_MIME_TYPE = new DataFormat("application/x-java-serialized-object");

    private Timeline scrolltimeline = new Timeline();
    private double scrollDirection = 0;
    private double coordinateYStartDragDrop = 0;
    private TreeTableView<ProjectTask> treeTable = new TreeTableView();
    private ScrollPane scrollPane = new ScrollPane();
    private ProjectData projectData = new ProjectData();
    private TreeChart treeChart;
    private boolean chartVisibility;
    private Stage primaryStage;
    private ProjectDataAction projectDataAction;

    public TreeTableProjectTask(Stage primaryStage) {

        this.primaryStage = primaryStage;
        scrollPane.setContent(treeTable);
        scrollPane.setVvalue(1);
        //scrollPane.setPrefViewportHeight(Control.USE_COMPUTED_SIZE);
        scrollPane.setFitToWidth(true);
        scrollPane.setFitToHeight(true);

        setTreeTable();
        treeChart = new TreeChart(treeTable);

        projectDataAction = new ProjectDataAction(treeTable, primaryStage);

        onResizableWindow();

    }

    public VBox getTreeTableWithChart() {

        HBox buttons = getButtonPane(treeTable);
        VBox vBox = new VBox(buttons, scrollPane);
        vBox.setStyle(TreeTableProjectTask.dafaultStyle);
        vBox.setFillWidth(true);

        return vBox;

    }

    public HBox getButtonPane(TreeTableView<ProjectTask> treeTable){

        Button addButton = new Button("Add");
        addButton.setOnAction(event -> addRow(treeTable));

        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(event -> deleteRows(treeTable));

        Button cloneProjectTask = new Button("Copy");
        cloneProjectTask.setOnAction(event -> cloneProjectTask(treeTable));

        Button calendarList = new Button("Calendar list");
        calendarList.setOnAction(event -> {
            new CalendarList(primaryStage);
        });

        Button loadTestTree = new Button("Load test data");
        loadTestTree.setOnAction(event -> {
            projectData.setupTestDate(treeTable);
            calculateWbs();
        });

        Button loadFromDatabase = new Button("Load from database");
        loadFromDatabase.setOnAction(event -> {
            projectDataAction.loadFromDatabase(event);
            calculateWbs();
        });

        Button saveToDatabase = new Button("Save to database");
        saveToDatabase.setOnAction(event -> {
            projectDataAction.saveToDatabase(event, projectData);
            calculateWbs();
        });

        Button moveUp = new Button("Move up");
        moveUp.setOnAction(event -> {
            moveUp();
        });

        Button showChart = new Button("Show chart");
        showChart.setOnAction(event -> {
            if (!chartVisibility) {
                scrollPane.setContent(new SplitPane(treeTable, treeChart.getRootWithCanvas()));
                showChart.setText("Hide chart");
            }else {
                scrollPane.setContent(treeTable);
                showChart.setText("Show chart");
            }
            chartVisibility = !chartVisibility;
        });

        return new HBox(20, addButton, cloneProjectTask, deleteButton, moveUp, calendarList,
                loadTestTree, loadFromDatabase, saveToDatabase, showChart);

    }

    public ProjectData getProjectData() {
        return projectData;
    }

    public void setTreeTable() {

        treeTable.setPrefHeight(Control.USE_COMPUTED_SIZE);
        treeTable.setEditable(true);
        treeTable.setSortPolicy(param -> false);

        treeTable.setOnKeyReleased(KeyEvent -> {
            if (KeyEvent.getCode() != KeyCode.DELETE || treeTable.getEditingCell() != null) {
                return;
            }
            deleteRows(treeTable);
        });
        //treeTable.setFixedCellSize(30);
        treeTable.setRowFactory(this::rowFactory);
        treeTable.getSelectionModel().selectFirst();
        BooleanProperty rootProperty = treeTable.showRootProperty();
        rootProperty.setValue(false);
        TreeTableView.TreeTableViewSelectionModel<ProjectTask> selection = treeTable.getSelectionModel();
        selection.setSelectionMode(SelectionMode.MULTIPLE);
        selection.setCellSelectionEnabled(true);

        setupScrolling();
        initializeTree();

    }

    public void addRow(TreeTableView<ProjectTask> treeTable) {

        if (treeTable.getRoot() == null){
            addNewRoot(treeTable);
        }

        TreeTableView.TreeTableViewSelectionModel<ProjectTask> selection = treeTable.getSelectionModel();
        TreeItem<ProjectTask> selectedRow = selection.getSelectedItem();

        TreeItem<ProjectTask> parent = treeTable.getRoot();
        boolean isSelectedRow = selectedRow != null && treeTable.getExpandedItemCount() != 0;
        if (isSelectedRow){
            parent = selectedRow.getParent();
        }

        TreeItem<ProjectTask> treeItem = getNewTaskForAdd();

        ObservableList<TreeItem<ProjectTask>> elementsOfParent = parent.getChildren();
        if (isSelectedRow){
            int indexOfSelectRow = elementsOfParent.indexOf(selectedRow);
            int indexOfNewRow = indexOfSelectRow + 1;
            elementsOfParent.add(indexOfNewRow, treeItem);
        } else{
            parent.getChildren().add(treeItem);
        }

        parent.setExpanded(true);

        editItem(treeItem, treeTable);

        // try to find row of new tree item
        int rowIndex = treeTable.getRow(treeItem);
        Set<Node> treeTableRowCell = treeTable.lookupAll(".tree-table-row-cell");
        TreeTableRow<?> row = null;
        for (Node tableRow : treeTableRowCell) {
            TreeTableRow<?> r = (TreeTableRow<?>) tableRow;
            if (r.getIndex() == rowIndex) {
                row = r;
                break;
            }
        };

        calculateWbs();

    }

    public void cloneProjectTask(TreeTableView<ProjectTask> treeTable) {

        TreeTableView.TreeTableViewSelectionModel<ProjectTask> selection = treeTable.getSelectionModel();
        if (selection.isEmpty()){
            return;
        }
        ObservableList<TreeItem<ProjectTask>> selectedItems = selection.getSelectedItems();
        cloneProjectTaskRecursively(selectedItems, null);

        calculateWbs();

    }

    public void deleteRows(TreeTableView<ProjectTask> treeTable) {

        TreeTableView.TreeTableViewSelectionModel<ProjectTask> selection = treeTable.getSelectionModel();

        if (selection.isEmpty()){
            return;
        }

        ObservableList<TreeItem<ProjectTask>> selectedRows = selection.getSelectedItems();
        if (selectedRows == null){
            return;
        }

        for (int i = selectedRows.size() - 1; i >= 0 ; i--) {
            TreeItem<ProjectTask> selectedRow = selectedRows.get(i);
            TreeItem<ProjectTask> parent = selectedRow.getParent();
            if (parent != null) {
                UUID id = selectedRow.getValue().getId();
                if (id != null) {
                    projectData.getDeletedProjectTasks().add(id);
                }
                parent.getChildren().remove(selectedRow);
            }
        }

        calculateWbs();

    }

    public void moveUp() {

        TreeTableView.TreeTableViewSelectionModel<ProjectTask> selection = treeTable.getSelectionModel();

        if (selection.isEmpty()){
            return;
        }

        ObservableList<TreeItem<ProjectTask>> selectedRows = selection.getSelectedItems();
        if (selectedRows == null){
            return;
        }

        for (TreeItem<ProjectTask> selectedRow: selectedRows) {

            TreeItem<ProjectTask> parent = selectedRow.getParent();
            if (parent == null) {
                continue;
            }
            ObservableList<TreeItem<ProjectTask>> children = parent.getChildren();

            int index = children.indexOf(selectedRow);
            if (index == -1 || index == 0) {
                continue;
            }
            children.remove(index);
            int newIndex = index - 1;
            children.add(newIndex, selectedRow);

            for (TreeItem<ProjectTask> treeItem: children) {
                projectData.addIdToChangedHierarchyElement(treeItem.getValue().getTempId());
            }

        }

        calculateWbs();

    }

    private void cloneProjectTaskRecursively(ObservableList<TreeItem<ProjectTask>> treeItems, TreeItem<ProjectTask> parent) {

        for (TreeItem<ProjectTask> treeItem: treeItems) {

            ProjectTask projectTask = treeItem.getValue();
            ProjectTask newProjectTask;
            try{
                newProjectTask = projectTask.clone();
            }
            catch(CloneNotSupportedException ex){
                newProjectTask = null;
                System.out.println("Cloneable not implemented");
            }
            if (newProjectTask == null){
                return;
            }
            TreeItem<ProjectTask> newTreeItem = new TreeItem<>(newProjectTask);
            TreeItem<ProjectTask> parentForAdd;
            if(parent == null) {
                parentForAdd = treeItem.getParent();

            } else {
                parentForAdd = parent;
            }
            parentForAdd.getChildren().add(newTreeItem);
            cloneProjectTaskRecursively(treeItem.getChildren(), newTreeItem);

        }

    }

    private TreeItem<ProjectTask> getNewTaskForAdd() {

        ProjectTask newProjectTask = new ProjectTask(LocalDate.now());
        TreeItem<ProjectTask> treeItem = new TreeItem(newProjectTask);

        return treeItem;

    }

    private void addNewRoot(TreeTableView<ProjectTask> treeTable) {

        TreeItem<ProjectTask> treeItem = getNewTaskForAdd();
        treeTable.setRoot(treeItem);

    }

    private void addTreeColumns(TreeTableView<ProjectTask> treeTable) {

        Callback<TreeTableColumn<ProjectTask, String>, TreeTableCell<ProjectTask, String>> cellFactory
                = (TreeTableColumn<ProjectTask, String> param) -> new TextEditingCell();
        TreeTableColumn<ProjectTask, String> nameCol = new TreeTableColumn("Name");
        nameCol.setPrefWidth(100);
        nameCol.setCellValueFactory(
                cellData -> cellData.getValue().getValue().getNameProperty()
        );
        nameCol.setCellFactory(cellFactory);
        nameCol.setOnEditCommit(event -> {

            String newValue = event.getNewValue();
            if (event.getOldValue().equals(newValue)) {
                return;
            }
            TreeItem<ProjectTask> treeItem = event.getRowValue();
            ProjectTask projectTask = treeItem.getValue();

            projectTask.setName(newValue);

            projectData.addChangedValueOfProjectTaskProperty(projectTask, "name", newValue);

        });

        TreeTableColumn<ProjectTask, String> wbsCol = new TreeTableColumn("WBS");
        wbsCol.setPrefWidth(40);
        wbsCol.setCellValueFactory(call -> call.getValue().getValue().getWbsProperty());

        Callback<TreeTableColumn<ProjectTask, Date>, TreeTableCell<ProjectTask, Date>> dateCellFactory
                = (TreeTableColumn<ProjectTask, Date> param) -> new DateEditingCell();
        TreeTableColumn<ProjectTask, Date> startDateCol = new TreeTableColumn("Start date");
        startDateCol.setCellValueFactory(cellData -> cellData.getValue().getValue().startDateProperty());
        startDateCol.setCellFactory(dateCellFactory);
        startDateCol.setOnEditCommit(event -> {

            Date newValue = event.getNewValue();
            if (newValue.equals(event.getOldValue())) {
                return;
            }
            TreeItem<ProjectTask> treeItem = event.getRowValue();
            ProjectTask projectTask = treeItem.getValue();

            //LocalDateTime newTime = newValue.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            if (newValue.equals(projectTask.getStartDate())) {
                return;
            }
            //projectTask.setStartDate(newTime);

            projectData.addChangedValueOfProjectTaskProperty(projectTask, "startDate", newValue);

        });

        TreeTableColumn<ProjectTask, Date> finishDateCol = new TreeTableColumn("Finish date");
        finishDateCol.setCellValueFactory(cellData -> cellData.getValue().getValue().finishDateProperty());
        finishDateCol.setCellFactory(dateCellFactory);
        finishDateCol.setOnEditCommit(event -> {

            Date newValue = event.getNewValue();
            if (newValue.equals(event.getOldValue())) {
                return;
            }
            TreeItem<ProjectTask> treeItem = event.getRowValue();
            ProjectTask projectTask = treeItem.getValue();

            //LocalDateTime newTime = newValue.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
            if (newValue.equals(projectTask.getFinishDate())) {
                return;
            }
            //projectTask.setFinishDate(newTime);

            projectData.addChangedValueOfProjectTaskProperty(projectTask, "finishDate", newValue);

        });

        // Add columns to the TreeTableView
        treeTable.getColumns().add(nameCol);
        treeTable.getColumns().add(wbsCol);
        treeTable.getColumns().add(startDateCol);
        treeTable.getColumns().add(finishDateCol);

    }

    private void editItem(TreeItem<ProjectTask> item, TreeTableView<ProjectTask> treeTable) {

        // Scroll to the new item
        int newRowIndex = treeTable.getRow(item);
        //treeTable.scrollTo(newRowIndex);

        // Put the first column in editing mode
        TreeTableColumn<ProjectTask, ?> firstCol = treeTable.getColumns().get(0);
        treeTable.getSelectionModel().select(item);
        treeTable.getFocusModel().focus(newRowIndex, firstCol);
        treeTable.edit(newRowIndex, firstCol);
    }

    private void initializeTree() {

        addTreeColumns(treeTable);

    }

    // WBS

    public void calculateWbs() {

        TreeItem<ProjectTask> rootTree = treeTable.getRoot();
        ObservableList<TreeItem<ProjectTask>> children = rootTree.getChildren();
        calculateWbsRecursively(children, "", 1);
        treeTable.refresh();

    }

    private void calculateWbsRecursively(ObservableList<TreeItem<ProjectTask>> treeItems, String parentWbs, int level) {

        int iterator = 1;
        for (TreeItem<ProjectTask> treeItem: treeItems) {

            String calculatedWbs = parentWbs + Integer.toString(iterator);
            treeItem.getValue().setWbs(calculatedWbs);
            int newLevel = level + 1;
            String newParentWbs = calculatedWbs + ".";
            ObservableList<TreeItem<ProjectTask>> treeItemsOfCurrentItem = treeItem.getChildren();
            calculateWbsRecursively(treeItemsOfCurrentItem, newParentWbs, newLevel);
            iterator++;
        }

    }

    // Scrolling

    private void setupScrolling() {

        scrolltimeline.setCycleCount(Timeline.INDEFINITE);
        scrolltimeline.getKeyFrames().add(new KeyFrame(Duration.millis(20), "Scoll", (ActionEvent) -> { dragScroll();}));

        treeTable.setOnDragExited(event -> {
            double coordinateY = event.getY();
            if (coordinateY - coordinateYStartDragDrop > 0) {
                scrollDirection = 1.0 / treeTable.getExpandedItemCount();
            }
            else {
                scrollDirection = -1.0 / treeTable.getExpandedItemCount();
            }
            scrolltimeline.play();
        });
        treeTable.setOnDragEntered(event -> {
            scrolltimeline.stop();
            coordinateYStartDragDrop = event.getY();
        });
        treeTable.setOnDragDone(event -> {
            scrolltimeline.stop();
        });

    }

    private void dragScroll() {

        ScrollBar sb = getVerticalScrollbar();
        if (sb != null) {
            double newValue = sb.getValue() + scrollDirection;
            newValue = Math.min(newValue, 1.0);
            newValue = Math.max(newValue, 0.0);
            sb.setValue(newValue);
        }

    }

    private ScrollBar getVerticalScrollbar() {

        ScrollBar result = null;
        //for (Node n : treeTable.lookupAll(".scroll-bar")) {
        for (Node n : scrollPane.getChildrenUnmodifiable()) {
            if (n instanceof ScrollBar) {
                ScrollBar bar = (ScrollBar) n;
                if (bar.getOrientation().equals(Orientation.VERTICAL)) {
                    result = bar;
                }
            }
        }
        return result;

    }

    private TreeTableRow<ProjectTask> rowFactory(TreeTableView<ProjectTask> view) {

        TreeTableRow<ProjectTask> row = new TreeTableRow<>();
        row.setOnDragDetected(event -> {
            if (!row.isEmpty()) {
                Dragboard db = row.startDragAndDrop(TransferMode.MOVE);
                db.setDragView(row.snapshot(null, null));
                ClipboardContent cc = new ClipboardContent();
                cc.put(SERIALIZED_MIME_TYPE, row.getIndex());
                db.setContent(cc);
                event.consume();
            }
        });

        row.setOnDragOver(event -> {
            Dragboard db = event.getDragboard();
            if (acceptable(db, row)) {
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
                event.consume();
            }
        });

        row.setOnDragDropped(event -> {
            Dragboard db = event.getDragboard();
            if (acceptable(db, row)) {
                int index = (Integer) db.getContent(SERIALIZED_MIME_TYPE);

                TreeItem<ProjectTask> item = treeTable.getTreeItem(index);
                item.getParent().getChildren().remove(item);
                TreeItem<ProjectTask> parentItem = getTarget(row);
                parentItem.getChildren().add(item);

                projectData.addIdToChangedHierarchyElement(item.getValue().getTempId());

                event.setDropCompleted(true);
                treeTable.getSelectionModel().select(item);
                event.consume();
                calculateWbs();
            }
        });

        return row;

    }

    private boolean acceptable(Dragboard db, TreeTableRow<ProjectTask> row) {
        boolean result = false;
        if (db.hasContent(SERIALIZED_MIME_TYPE)) {
            int index = (Integer) db.getContent(SERIALIZED_MIME_TYPE);
            if (row.getIndex() != index) {
                TreeItem target = getTarget(row);
                TreeItem item = treeTable.getTreeItem(index);
                result = !isParent(item, target);
            }
        }
        return result;
    }

    private TreeItem getTarget(TreeTableRow<ProjectTask> row) {
        TreeItem target = treeTable.getRoot();
        if (!row.isEmpty()) {
            target = row.getTreeItem();
        }
        return target;
    }

    // prevent loops in the tree
    private boolean isParent(TreeItem parent, TreeItem child) {
        boolean result = false;
        while (!result && child != null) {
            result = child.getParent() == parent;
            child = child.getParent();
        }
        return result;
    }

    // Resizable

    private void onResizableWindow() {
        primaryStage.heightProperty().addListener(
                (observable, oldValue, newValue) -> {
                    scrollPane.setPrefViewportHeight((Double) newValue);
                    //treeChart.setHeightCanvas(treeTable.getHeight());
                });
    }

}
