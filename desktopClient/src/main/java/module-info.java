module Project.management.desktopClient.main {

    requires javafx.controls;
    requires javafx.fxml;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.datatype.jsr310;
    requires com.fasterxml.jackson.databind;
    requires spring.web;
    requires static lombok;
    requires org.controlsfx.controls;

    opens com.PM.Calendar;

    exports com.PM;
    exports com.PM.ProjectStructure;
    exports com.PM.Calendar;

}