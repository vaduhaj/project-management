package com.PM.Calendar;

import com.PM.ProjectStructure.ProjectData;
import com.PM.TreeTableProjectTask;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.BigDecimalStringConverter;
import org.controlsfx.dialog.ExceptionDialog;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.util.*;


public class CalendarEditForm {

    private Stage stage;
    private Scene scene;
    private UUID objectId;
    private final String url = String.format(ProjectData.resourceUrl, "Calendar/");
    private Map currentObject;
    private TableView<Map<String, Object>> dayOfWeekSettings;
    private boolean continueLoad = true;
    private final RestTemplate restTemplate = new RestTemplate();

    public CalendarEditForm(Stage primaryStage, UUID objectId){

        this.objectId = objectId;
        loadFromServer();

        if (!continueLoad) {
            return;
        }

        Button save = new Button("Save");
        save.setOnAction(this::saveObject);

        Node properties = getProperties();
        StackPane secondaryLayout = new StackPane();

        secondaryLayout.getChildren().add(properties);

        Insets insets = new Insets(10, 5, 10, 5);

        prepareDayOfWeekSettingsTable();
        fillDayOfWeekSettingsTable();

        VBox vBox = new VBox(save, secondaryLayout, dayOfWeekSettings);
        vBox.setPadding(new Insets(10, 10, 10, 10));
        vBox.setMaxHeight(Double.MAX_VALUE);
        VBox.setMargin(save, insets);
        vBox.setStyle(TreeTableProjectTask.dafaultStyle);
        scene = new Scene(vBox);

        stage = new Stage();
        stage.setTitle("Calendar");
        stage.setScene(scene);

        stage.initModality(Modality.WINDOW_MODAL);

        stage.initOwner(primaryStage);

        stage.setX(primaryStage.getX() + 200);
        stage.setY(primaryStage.getY() + 100);
        stage.show();

    }

    private void loadFromServer() {

        UUID id = this.objectId;
        if (id == null) {
            id = new UUID(0 , 0 );
        }

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("id", "{id}")
                .encode()
                .toUriString();

        Map<String, UUID> params = new HashMap<>();
        params.put("id", id);

        HttpEntity<String> response;
        try {
            response = restTemplate.exchange(urlTemplate, HttpMethod.GET, entity, String.class, params);
        } catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc){

            ExceptionDialog dlg = new ExceptionDialog(new Exception("Failed to get data", httpClientOrServerExc));
            dlg.initOwner(stage);
            dlg.show();
            continueLoad = false;
            return;
        }

        try {
            String responseString = response.getBody();
            currentObject = new ObjectMapper().readValue(responseString, Map.class);
        } catch (Exception e) {
            ExceptionDialog dlg = new ExceptionDialog(e);
            dlg.initOwner(stage);
            dlg.show();
            continueLoad = false;
            return;
        }

        castMapValuesToProperties();

    }

    private void saveObject(ActionEvent actionEvent) {

        ResponseEntity<String> response;
        Map savedObject = getSavedObjectWithoutProperties();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {

            String calendarJson = new ObjectMapper().writerFor(Map.class).writeValueAsString(savedObject);
            HttpEntity<String> entity = new HttpEntity<>(calendarJson, headers);
            response = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
        } catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {
            ExceptionDialog dlg = new ExceptionDialog(new Exception("Failed to save data", httpClientOrServerExc));
            dlg.initOwner(stage);
            dlg.show();
            return;
        } catch (JsonProcessingException e) {
            ExceptionDialog dlg = new ExceptionDialog(new Exception("Failed to save data", e));
            dlg.initOwner(stage);
            dlg.show();
            return;
        }

        try {

            String responseString = response.getBody();
            savedObject = new ObjectMapper().readValue(responseString, Map.class);
            currentObject = savedObject;
            String id = (String) savedObject.get("id");
            if (id != null) {
                objectId = UUID.fromString(id);
            }
            castMapValuesToProperties();
            fillDayOfWeekSettingsTable ();
            dayOfWeekSettings.refresh();
        } catch (Exception e){
            ExceptionDialog dlg = new ExceptionDialog(e);
            dlg.initOwner(stage);
            dlg.show();
            return;
        }

    }

    private void castMapValuesToProperties() {
        List<Map<String, Object>> rowsTable = (List<Map<String, Object>>) currentObject.get("daysOfWeekSettings");

        if (rowsTable == null) {
            return;
        }

        rowsTable.forEach(rowTable -> {
            String value = rowTable.get("countHours").toString();
            BigDecimal newValue = new BigDecimal(value);
            SimpleObjectProperty<BigDecimal> newValueProperty = new SimpleObjectProperty<>(newValue);
            rowTable.put("countHours", newValueProperty);
        });
    }

    private Map getSavedObjectWithoutProperties() {

        Map savedValue = new HashMap<>(currentObject);
        List<Map<String, Object>> rowsTable = (List<Map<String, Object>>) savedValue.get("daysOfWeekSettings");
        rowsTable.forEach(rowTable -> {
            Object valueObject = rowTable.get("countHours");
            Object value = null;
            if (valueObject instanceof SimpleObjectProperty) {
                SimpleObjectProperty<BigDecimal> property = (SimpleObjectProperty<BigDecimal>) valueObject;
                value = property.getValue();
            }else if (valueObject instanceof BigDecimal) {
                value = valueObject;
            }
            rowTable.put("countHours", value);
        });

        return savedValue;
    }

    private void prepareDayOfWeekSettingsTable() {

        dayOfWeekSettings = new TableView<>();
        dayOfWeekSettings.setSortPolicy(param -> false);
        dayOfWeekSettings.setEditable(true);

//        TableColumn<Map<String, Object>, Integer> colId = new TableColumn("№");
//        colId.setCellValueFactory(callData -> new SimpleObjectProperty<Integer>((Integer) callData.getValue().get("id")));

        TableColumn<Map<String, Object>, String> colDayOfWeek = new TableColumn("Day of week");
        colDayOfWeek.setCellValueFactory(callData -> new SimpleObjectProperty<String>((String) callData.getValue().get("dayOfWeekString")));

        TableColumn<Map<String, Object>, BigDecimal> colCountHours = new TableColumn("Count hours");
        colCountHours.setCellValueFactory(callData -> (SimpleObjectProperty<BigDecimal>) callData.getValue().get("countHours"));
        StringConverter converter = new BigDecimalStringConverter();
        colCountHours.setCellFactory(TextFieldTableCell.forTableColumn(converter));

        dayOfWeekSettings.getColumns().addAll(colDayOfWeek, colCountHours);

    }

    private void fillDayOfWeekSettingsTable() {

        ObservableList<Map<String, Object>> items =
                FXCollections.observableArrayList();

        List<Map<String, Object>> rowsTable = (List<Map<String, Object>>) currentObject.get("daysOfWeekSettings");//calendar.getDaysOfWeekSettings();
        items.addAll(rowsTable);
        dayOfWeekSettings.setItems(items);

    }

    private Node getProperties(){

        Label lblName = new Label();
        lblName.setText("Name: ");
        TextField textFieldName = new TextField();
        textFieldName.setPrefColumnCount(11);
        final String nameKey = "name";
        textFieldName.setText((String) currentObject.get(nameKey));
        textFieldName.setOnAction(event -> {
            String text = textFieldName.getText();
            if (text.equals("")) {
                textFieldName.setText((String) currentObject.get(nameKey));
                return;
            }
            currentObject.put(nameKey, text);
        });
        textFieldName.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                currentObject.put(nameKey, textFieldName.getText());
            }
        });

        HBox nameHBox = new HBox(lblName, textFieldName);

        Label lblCalendarSettings = new Label();
        lblCalendarSettings.setText("Calendar settings: ");
        ObservableList<CalendarSettings> settings = FXCollections.observableArrayList(CalendarSettings.values());
        ComboBox<CalendarSettings> settingsComboBox = new ComboBox(settings);
        final String settingKey = "setting";
        settingsComboBox.setValue(CalendarSettings.valueOf((String) currentObject.get(settingKey)));
        settingsComboBox.setOnAction(event -> {
            CalendarSettings newCalendarSettings = settingsComboBox.getValue();
            currentObject.put(settingKey, newCalendarSettings);
        });
        HBox calendarSettingsHBox = new HBox(lblCalendarSettings, settingsComboBox);

        VBox propertiesBox = new VBox(nameHBox, calendarSettingsHBox);

        return propertiesBox;

    }


}
