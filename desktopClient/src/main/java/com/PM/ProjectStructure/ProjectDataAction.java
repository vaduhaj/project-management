package com.PM.ProjectStructure;

import javafx.event.ActionEvent;
import javafx.scene.control.TreeTableView;
import javafx.stage.Stage;
import org.controlsfx.dialog.ExceptionDialog;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ProjectDataAction {

    private TreeTableView treeTable;
    private Stage primaryStage;

    public ProjectDataAction(TreeTableView treeTable, Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.treeTable = treeTable;
    }

    public void loadFromDatabase(ActionEvent actionEvent) {

        RestTemplate restTemplate = new RestTemplate();
        final String url = String.format(ProjectData.resourceUrl, "ProjectData/");
        try {
            ResponseEntity<ProjectDTO> response =
                    restTemplate.getForEntity(url, ProjectDTO.class);
            ProjectDTO projectDTO = response.getBody();
            ProjectData projectData = new ProjectData(projectDTO);
            projectData.setupNewTree(treeTable);
        } catch (Exception e){
            ExceptionDialog dlg = new ExceptionDialog(e);
            dlg.initOwner(primaryStage);
            dlg.show();
        }

    }

    public void saveToDatabase(ActionEvent actionEvent, ProjectData projectData) {

        projectData.fillByTreeTableView(treeTable);
        ProjectDTO projectDTO = new ProjectDTO(projectData);
        final String url = String.format(ProjectData.resourceUrl, "ProjectData/");
        RestTemplate restTemplate = new RestTemplate();
        try {

            HttpHeaders httpHeaders = restTemplate.headForHeaders(url);
            HttpEntity<ProjectDTO> entity = new HttpEntity<>(projectDTO, httpHeaders);
            ResponseEntity<ProjectDTO> response =
                    restTemplate.exchange(url, HttpMethod.PUT, entity, ProjectDTO.class);

            projectDTO = response.getBody();
            ProjectData projectDataResponse = new ProjectData(projectDTO);
            projectDataResponse.setupNewTree(treeTable);

        } catch (Exception e){
            ExceptionDialog dlg = new ExceptionDialog(e);
            dlg.initOwner(primaryStage);
            dlg.show();
        }

    }

}
