package server.entities;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Data
@Entity
@Table(name = "project_tasks_hierarchy")
public class ProjectTasksHierarchy implements Serializable {

    public static final int ROOT_ID = 0;

    @Id
    @Column(name = "id")
    private UUID id;

    @Column(name = "parent_id")
    private UUID parentId;

    @Transient
    private int tempId;
    @Transient
    private int tempParentId;

//    @Column(name = "wbs")
//    private String wbs;

    @Column(name = "level_order")
    private Integer levelOrder;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProjectTasksHierarchy)) return false;
        ProjectTasksHierarchy hierarchy = (ProjectTasksHierarchy) o;
        return getId().equals(hierarchy.getId()) && getTempId() == hierarchy.getTempId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTempId());
    }

}
