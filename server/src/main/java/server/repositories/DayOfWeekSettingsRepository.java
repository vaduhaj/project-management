package server.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import server.entities.Calendar.DayOfWeekSettings;

import java.util.List;
import java.util.UUID;

public interface DayOfWeekSettingsRepository extends JpaRepository<DayOfWeekSettings, Integer> {
    List<DayOfWeekSettings> findByCalendarId(UUID calendarId);
}
