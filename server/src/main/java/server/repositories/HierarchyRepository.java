package server.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import server.entities.ProjectTasksHierarchy;

import java.util.List;
import java.util.UUID;

public interface HierarchyRepository extends JpaRepository<ProjectTasksHierarchy, UUID> {

//    List<ProjectTasksHierarchy> findAllByOrderByWbsAsc();
    List<ProjectTasksHierarchy> findAllByOrderByLevelOrderAsc();
    // Need function
//    List<ProjectTasksHierarchy> findAllByParentIdOrderByLevelOrderAsc(Iterable ids);

    void deleteAllById(Iterable ids);

    List<ProjectTasksHierarchy> findAllById(Iterable ids);

}

