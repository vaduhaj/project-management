package com.PM;

import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.Chart;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeTableView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class TreeChart {

    TreeTableView treeTable;
    Canvas canvas;
    VBox vBox;

    public TreeChart(TreeTableView treeTable) {
        this.treeTable = treeTable;
        canvas = new Canvas(400, treeTable.getHeight());
        GraphicsContext gc = canvas.getGraphicsContext2D();

        gc.setStroke(Color.MEDIUMAQUAMARINE);
        gc.setLineWidth(18.88);
        gc.strokeLine(0, 37, 40, 37);

        GridPane gridpane = new GridPane();
        gridpane.getColumnConstraints().add(new ColumnConstraints(80));
        gridpane.getColumnConstraints().add(new ColumnConstraints(80));
        gridpane.getColumnConstraints().add(new ColumnConstraints(80));
        gridpane.setGridLinesVisible(true);

        Label first = new Label("First");
        Label second = new Label("Second");
        Label third = new Label("Third");

        gridpane.setColumnIndex(first, 0);
        gridpane.setColumnIndex(second, 1);
        gridpane.setColumnIndex(third, 2);
        gridpane.getChildren().addAll(first, second, third);

        vBox = new VBox();
        ObservableList<Node> childrenVBox = vBox.getChildren();
        childrenVBox.add(gridpane);
        childrenVBox.add(canvas);
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public void setHeightCanvas(double height) {
        canvas.setHeight(height);
    }

    public Node getRootWithCanvas(){

        return vBox;

    }

}
