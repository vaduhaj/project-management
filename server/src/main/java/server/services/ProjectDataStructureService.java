package server.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import server.ProjectStructure.ProjectDTO;
import server.ProjectStructure.ProjectData;
import server.repositories.HierarchyRepository;
import server.repositories.ProjectTaskRepository;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

@Service
public class ProjectDataStructureService {

    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;
    private ProjectTaskRepository projectTaskRepository;
    private HierarchyRepository hierarchyRepository;

    @Autowired
    public void setProjectTaskRepository(ProjectTaskRepository projectTaskRepository){
        this.projectTaskRepository = projectTaskRepository;
    }

    @Autowired
    public void setHierarchyRepository(HierarchyRepository hierarchyRepository){
        this.hierarchyRepository = hierarchyRepository;
    }

    public ProjectDTO getProjectDTO(){

        try {
            ProjectData projectData = new ProjectData();
            projectData.setProjectTasks(projectTaskRepository.findAll());
            projectData.setHierarchy(hierarchyRepository.findAllByOrderByLevelOrderAsc());
            projectData.fillTempIds();
            return new ProjectDTO(projectData);
        } catch (Exception e){
            return new ProjectDTO(e);
        }

    }


    public ProjectDTO saveProjectDTO(ProjectDTO projectDTO) {

        try {
            ProjectData projectData = new ProjectData(projectDTO);
            projectData.setProjectTaskRepository(projectTaskRepository);
            projectData.setHierarchyRepository(hierarchyRepository);
            projectData.setEntityManagerFactory(entityManagerFactory);
            projectData.prepareDataForStorage();
            saveProjectData(projectData);
        } catch (Exception e){
            return new ProjectDTO(e);
        }

        return getProjectDTO();

    }

    @Transactional
    private void saveProjectData(ProjectData projectData) {

        projectData.saveData();

    }

}
