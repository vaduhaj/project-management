package com.PM.Calendar;

import com.PM.ProjectStructure.ProjectData;
import com.PM.TreeTableProjectTask;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.MapValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.controlsfx.dialog.ExceptionDialog;
import org.springframework.http.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;
import java.util.stream.Collectors;

public class CalendarList {

    private Stage stage;
    private TableView<Map<String, String>> calendars;
    private static final String resourceUrl = String.format(ProjectData.resourceUrl, "Calendars/");
    private boolean continueLoad = true;
    List<Map<String, String>> rowsTable;
    private static final String fieldId = "id";
    private static final String fieldIds = "ids";

    public CalendarList(Stage primaryStage){

        setTableView();

        loadFromServer();

        if (!continueLoad) {
            return;
        }

        VBox vBox = getTableView();

        Scene scene = new Scene(vBox);

        stage = new Stage();
        stage.setTitle("Calendars");
        stage.setScene(scene);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(primaryStage);
        stage.setX(primaryStage.getX() + 200);
        stage.setY(primaryStage.getY() + 100);

        stage.show();

    }

    private void loadFromServer() {

        RestTemplate restTemplate = new RestTemplate();

        try {
            ResponseEntity<List> response =
                    restTemplate.getForEntity(resourceUrl, List.class);
            rowsTable = response.getBody();
        } catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc){

            ExceptionDialog dlg = new ExceptionDialog(new Exception("Failed to get data", httpClientOrServerExc));
            dlg.initOwner(stage);
            dlg.show();
            continueLoad = false;
            return;
        }

        fillTableView();

    }

    private void fillTableView(){

        ObservableList<Map<String, String>> items =
                FXCollections.observableArrayList();

        items.addAll(rowsTable);
        calendars.setItems(items);

    }

    private TableRow<Map<String, String>> rowFactory(TableView<Map<String, String>> view) {

        TableRow<Map<String, String>> row = new TableRow<>();
        row.setOnMouseClicked(event -> {
            if (event.getClickCount() != 2 || row.isEmpty()){
                return;
            }
            Map<String, String> tableRow = row.getItem();
            new CalendarEditForm(stage, UUID.fromString(tableRow.get(fieldId)));

        });

        return row;

    }

    private void setTableView(){

        calendars = new TableView();
        calendars.setPrefHeight(Control.USE_COMPUTED_SIZE);
        calendars.setEditable(false);
        calendars.maxHeight(Double.MAX_VALUE);
        calendars.setTableMenuButtonVisible(true);
        calendars.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        TableColumn<Map<String, String>, String> nameCol = new TableColumn("Name");
        nameCol.setCellValueFactory(new MapValueFactory("name"));
        TableColumn<Map<String, String>, String> nameSettingsCol = new TableColumn("Settings");
        nameSettingsCol.setCellValueFactory(new MapValueFactory("settingString"));

        calendars.getColumns().addAll(nameCol, nameSettingsCol);
        calendars.setRowFactory(this::rowFactory);

    }

    private HBox getActionPanel() {

//        Collection<Action> actions = new ArrayList<>();
//        Action create = new Action("Create"){
//            {setEventHandler(this::saveAction); }
//            private void saveAction(ActionEvent ae) {
//                new CalendarEditForm(stage, null);
//            }
//        };
//        Action delete = new Action("Delete"){
//            {setEventHandler(this::deleteAction); }
//            private void deleteAction(ActionEvent ae) {
//                final String url = String.format(resourceUrl, "Calendars/");
//                RestTemplate restTemplate = new RestTemplate();
//            }
//        };
//        actions.add(create);
//        Control actionUtils = ActionUtils.createToolBar(actions, ActionUtils.ActionTextBehavior.SHOW);
//
//        return actionUtils;

        Button create = new Button("Create");
        create.setOnAction(event ->  new CalendarEditForm(stage, null));
        Button delete = new Button("Delete");
        delete.setOnAction(this::deleteSelectedRows);
        Button refresh = new Button("Refresh");
        refresh.setOnAction(this::refreshData);

        HBox hBox = new HBox(20, create, delete, refresh);
        hBox.setMaxWidth(Double.MAX_VALUE);
        hBox.setFillHeight(true);

        return hBox;

    }

    private void refreshData(ActionEvent actionEvent) {

        loadFromServer();

    }

    private void showErrorDialog(String message, Exception exception) {

        Exception newException = new Exception(message, exception);
        ExceptionDialog dlg = new ExceptionDialog(newException);
        dlg.initOwner(stage);
        dlg.show();

    }

    private void deleteSelectedRows(ActionEvent actionEvent) {

        ObservableList<Map<String, String>> selectedItems = calendars.getSelectionModel().getSelectedItems();

        if (selectedItems == null || selectedItems.size() == 0) {
            return;
        }

        List<UUID> ids = selectedItems.stream()
//        Map<String, UUID> ids = selectedItems.stream()
//                .collect(Collectors.toMap(
//                        stringStringMap -> "fieldId",
//                        stringStringMap -> UUID.fromString((String) stringStringMap.get(fieldId))));
                .map(stringStringMap -> UUID.fromString((String) stringStringMap.get(fieldId)))
                .collect(Collectors.toList());

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        String urlTemplate = UriComponentsBuilder.fromHttpUrl(resourceUrl)
                .queryParam(fieldIds, "{" + fieldIds + "}")
                .encode()
                .toUriString();

        String parameterJson = "";
        try {
            parameterJson = new ObjectMapper().writerFor(List.class).writeValueAsString(ids);
        } catch (JsonProcessingException e) {
            showErrorDialog("Failed to convert ids", e);
            return;
        }

        Map<String, String> params = new HashMap<>();
        params.put(fieldIds, parameterJson);

        RestTemplate restTemplate = new RestTemplate();

        try {

            restTemplate.exchange(urlTemplate, HttpMethod.DELETE, entity, String.class, params);

        } catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {

            showErrorDialog("Failed to delete item.", httpClientOrServerExc);
            return;

        } catch (Exception e){

            showErrorDialog("Failed to delete item.", e);
            return;

        }

        loadFromServer();
        //TODO Scroll to the first one coming from remote table rows treeTable.scrollTo(newRowIndex)
        selectedItems.clear();

    }

    private VBox getTableView() {

//        StackPane secondaryLayout = new StackPane();
//        secondaryLayout.getChildren().add(calendars);
        HBox actionPanel = getActionPanel();
        Insets insets = new Insets(5, 5, 5, 5);
        VBox vBox = new VBox(actionPanel, calendars);
        VBox.setMargin(actionPanel, insets);
        vBox.setStyle(TreeTableProjectTask.dafaultStyle);
        vBox.setMaxHeight(Double.MAX_VALUE);
        vBox.setPrefHeight(Double.MAX_VALUE);

        return vBox;

    }

}
