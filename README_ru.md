Project management

Настройки:
1. Клиент создает запросы к 8080 порту, изменить порт можно, если изменить его в пути, указанным в классе desktopClient/src/main/java/com/PM/ProjectStructure/ProjectData.java константа resourceUrl.
2. Порт сервера и настройки подключения к базе данных можно изменить в файле server/src/main/resources/application.properties
3. В PostgreSQL нужно создать базу данных "PM" в схеме public выполнить SQL-скрипты в порядке следования из server/src/main/resources/db/migration
Имя базы данных и схему можно изменить, изменив spring.datasource.url в application.properties из п.2
4. По умолчанию для доступа к базе данных используется пользователь PostgreSQL - "postgres", пароль 123. Имя пользователя и пароль можно также изменить в файле application.properties из п.2

Клиент запускается из Gradle по пути Project management/desktopClient/Tasks/application/run

Сервер запускается прямо из класса server/src/main/java/server/MainApp.java
