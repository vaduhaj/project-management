package server.ProjectStructure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import server.entities.ProjectTask;
import server.entities.ProjectTasksHierarchy;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectDTO {

    private List<ProjectTasksHierarchy> hierarchy;
    private List<ProjectTask> projectTasks;
    private List<ProjectTask> newProjectTasks;
    private Map<UUID, Map<String, Object>> changedProjectTasks;
    private List<UUID> deletedProjectTasks;
    private Exception exception;

    public ProjectDTO(){

    }

    public ProjectDTO(ProjectData projectData) {

        this.hierarchy = projectData.getHierarchy();
        this.projectTasks = projectData.getProjectTasks();
        this.newProjectTasks = projectData.getNewProjectTasks();
        this.changedProjectTasks = projectData.getChangedProjectTasks();
        this.deletedProjectTasks = projectData.getDeletedProjectTasks();

    }

    public ProjectDTO(Exception exception) {
        this.exception = exception;
    }
}
