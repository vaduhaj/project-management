package com.PM.EditingCells;

import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class TextEditingCell<T> extends TreeTableCell<T, String> {

    private TextField textField;

    public TextEditingCell() {
    }

    @Override
    public void startEdit() {
        if (!isEmpty()) {
            super.startEdit();
            createTextField();
            setText(null);
            setGraphic(textField);
            textField.selectAll();
        }
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText(getItem());
        setGraphic(null);
    }

    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);

        if (empty) {
            setText(item);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (textField != null) {
                    textField.setText(getString());
                }
                setText(null);
                setGraphic(textField);
            } else {
                setText(getString());
                setGraphic(null);
            }
        }
    }

    @Override
    public void commitEdit(String newValue) {

        if (! isEditing() && ! newValue.equals(getItem())) {
            TreeTableView<T> treeTable = getTreeTableView();
            if (treeTable != null) {
                TreeTableColumn<T, String> column = getTableColumn();

                TreeTableColumn.CellEditEvent<T, String> event = new TreeTableColumn.CellEditEvent<T, String>(treeTable,
                        new TreeTablePosition<T, String>(treeTable, getIndex(), column),
                        TreeTableColumn.editCommitEvent(), newValue);
                Event.fireEvent(column, event);
            }
        }

        super.commitEdit(newValue);

    }

    private void createTextField() {
        textField = new TextField(getString());
        textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        textField.setOnAction((e) -> commitEdit(textField.getText()));
        textField.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!newValue) {
                commitEdit(textField.getText());
            }
        });
        textField.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.ESCAPE) {
                textField.setText(getItem());
                cancelEdit();
                event.consume();
            }
        });
    }

    private String getString() {
        return getItem() == null ? "" : getItem();
    }

}
