package server.entities.Calendar;

import java.util.UUID;

public interface CalendarRowTable {

    UUID getId();
    String getName();
    CalendarSettings getSetting();

}
