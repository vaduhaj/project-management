package server.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import server.entities.Calendar.Calendar;
import server.services.CalendarService;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class CalendarController {

    private CalendarService calendarService;

    @Autowired
    public void setCalendarService(CalendarService calendarService){
        this.calendarService = calendarService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/Calendars")
    public List<Calendar> findAllCalendars() {
        return calendarService.getListCalendarRowTable();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/Calendars")
    @ResponseStatus(HttpStatus.OK)
    public void deleteCalendars(@RequestParam(name = "ids") String idsString) throws JsonProcessingException {

        List<String> stringIds = new ObjectMapper().readValue(idsString, List.class);
        List<UUID> ids = stringIds.stream().map(string -> UUID.fromString(string)).collect(Collectors.toList());
        calendarService.deleteCalendars(ids);

    }

    @RequestMapping(method = RequestMethod.GET, value = "/Calendar")
    public Calendar getCalendar(@RequestParam(name = "id") UUID id) {

        if (id.equals(new UUID(0, 0))) {
            return Calendar.getNewCalendar();
        }
        return calendarService.getCalendar(id);
    }

    @RequestMapping(method = RequestMethod.HEAD, value = "/Calendar")
    public Calendar getCalendarForHead() {
        return new Calendar();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/Calendar")
    public Calendar saveCalendar(@RequestBody Calendar calendar) {
        return calendarService.saveCalendar(calendar);
    }

}
