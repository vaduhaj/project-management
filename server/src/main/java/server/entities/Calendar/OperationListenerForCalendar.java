package server.entities.Calendar;

import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import java.util.UUID;

public class OperationListenerForCalendar {

    @PostLoad
    public void postLoad(Calendar calendar) {

        calendar.setSettingString(calendar.getSetting().toString());

    }

    @PrePersist
    public void prePersist(Calendar calendar) {

        if (calendar.getId() == null || calendar.getId().equals(new UUID(0, 0))) {
            final UUID newUUID = UUID.randomUUID();
            calendar.setId(newUUID);
        }

    }

}
