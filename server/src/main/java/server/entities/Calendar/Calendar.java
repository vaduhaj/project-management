package server.entities.Calendar;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
@EntityListeners(OperationListenerForCalendar.class)
@NoArgsConstructor
@Table(name = "calendars")
public class Calendar implements Serializable, CalendarRowTable {

    @Id
    @Getter
    @Setter
    private UUID id;

    @Version
    @Getter
    private Integer version;

    @Transient
    @Getter
    @Setter
    private Exception exception;

    @Getter
    @Setter
    private String name = "";

    @Getter
    @Setter
    @Enumerated(EnumType.STRING)
//    @ManyToOne
//    @JoinColumn(name = "id")
    private CalendarSettings setting;

    @Getter
    @Setter
    @Transient
    private String settingString;

//    @OneToMany
//    @JoinColumn(name = "calendar_id", referencedColumnName = "id")
    @Getter
    @Setter
    @OneToMany(mappedBy = "calendar", fetch = FetchType.EAGER,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})//, cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("dayOfWeek ASC")
    private List<DayOfWeekSettings> daysOfWeekSettings;

//    private ArrayList<ExceptionDay> exceptionDaysSettings;

    public Calendar(Exception exception){
        this.exception = exception;
    }

    public Calendar(String name){
        this.name = name;
    }

    public static Calendar getNewCalendar() {

        Calendar newCalendar = new Calendar();
        newCalendar.setting = CalendarSettings.EIGHTHOURWORKINGDAY;
        newCalendar.settingString = newCalendar.setting.toString();
        newCalendar.daysOfWeekSettings = newCalendar.setting.getDaysOfWeekSettings();

        return newCalendar;

    }

    @Override
    public int hashCode() {
        return Objects.hash(id, version);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (!(o instanceof Calendar)) {
            return false;
        }

        Calendar that = (Calendar) o;

        return getId().equals(that.getId()) && getVersion().equals(that.getVersion()) ;
    }

    @Override
    public String toString() {
        return name;
    }

}

