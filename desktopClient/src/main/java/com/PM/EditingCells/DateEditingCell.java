package com.PM.EditingCells;

import javafx.event.Event;
import javafx.scene.control.*;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;


public class DateEditingCell<T> extends TreeTableCell<T, Date> {

    private DatePicker datePicker;

    public DateEditingCell() {
    }

    @Override
    public void startEdit() {
        if (!isEmpty()) {
            super.startEdit();
            createDatePicker();
            setText(null);
            setGraphic(datePicker);
        }
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();

        setText(getDate().toString());
        setGraphic(null);
    }

    @Override
    public void updateItem(Date item, boolean empty) {

        super.updateItem(item, empty);

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {
            if (isEditing()) {
                if (datePicker != null) {
                    datePicker.setValue(getDate());
                }
                setText(null);
                setGraphic(datePicker);
            } else {
                setText(getDate().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)));//getDate().toString());
                setGraphic(null);
            }
        }
    }

    @Override
    public void commitEdit(Date newValue) {

        if (! isEditing() && ! newValue.equals(getItem())) {
            TreeTableView<T> treeTable = getTreeTableView();
            if (treeTable != null) {
                TreeTableColumn<T, Date> column = getTableColumn();

                TreeTableColumn.CellEditEvent<T, Date> event = new TreeTableColumn.CellEditEvent<>(treeTable,
                        new TreeTablePosition<>(treeTable, getIndex(), column),
                        TreeTableColumn.editCommitEvent(), newValue);
                Event.fireEvent(column, event);
            }
        }

        super.commitEdit(newValue);

    }

    private void createDatePicker() {
        datePicker = new DatePicker(getDate());
        datePicker.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
        datePicker.setOnAction((e) -> commitEdit(Date.from(datePicker.getValue().atStartOfDay(ZoneOffset.ofHours(0)).toInstant())));//datePicker.getValue()));
        datePicker.focusedProperty().addListener((obs, wasFocused, isNowFocused) -> {
            if (! isNowFocused) {
                commitEdit(Date.from(datePicker.getValue().atStartOfDay(ZoneOffset.ofHours(0)).toInstant()));//getDate());
            }
        });
    }

    private LocalDate getDate() {
        Date currentValue = getItem();
        if (currentValue == null) {
            return LocalDate.now();
        }
        return currentValue.toInstant().atZone(ZoneOffset.ofHours(0)).toLocalDate();
    }

}