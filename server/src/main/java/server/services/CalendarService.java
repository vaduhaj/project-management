package server.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import server.entities.Calendar.Calendar;
import server.repositories.CalendarRepository;
import server.repositories.CalendarRowTableRepository;
import server.repositories.DayOfWeekSettingsRepository;

import java.util.List;
import java.util.UUID;

@Service
public class CalendarService {

    private CalendarRepository calendarRepository;
    private CalendarRowTableRepository calendarRowTableRepository;
    private DayOfWeekSettingsRepository dayOfWeekSettingsRepository;

    @Autowired
    public void setCalendarRepository(CalendarRepository calendarRepository){
        this.calendarRepository = calendarRepository;
    }

    @Autowired
    public void setCalendarRowTableRepository(CalendarRowTableRepository calendarRowTableRepository){
        this.calendarRowTableRepository = calendarRowTableRepository;
    }

    @Autowired
    public void setDayOfWeekSettingsRepository(DayOfWeekSettingsRepository dayOfWeekSettingsRepository){
        this.dayOfWeekSettingsRepository = dayOfWeekSettingsRepository;
    }

    public List<Calendar> getListCalendarRowTable() {
        return calendarRepository.findAll();
    }

    public Calendar getCalendar(UUID id) {
        Calendar foundedCalendar = calendarRepository.findById(id).get();
        return foundedCalendar;
    }

    public Calendar saveCalendar(Calendar calendar) {

        calendar.getDaysOfWeekSettings().forEach(dayOfWeekSettings -> dayOfWeekSettings.setCalendar(calendar));
        return calendarRepository.save(calendar);

    }

    public void deleteCalendars(List<UUID> ids) {

        calendarRepository.deleteAllById(ids);

    }

}
