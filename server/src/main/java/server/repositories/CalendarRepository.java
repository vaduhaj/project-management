package server.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import server.entities.Calendar.Calendar;

import java.util.List;
import java.util.UUID;

public interface CalendarRepository extends JpaRepository<Calendar, UUID> {

    List<Calendar> findAll();

}
