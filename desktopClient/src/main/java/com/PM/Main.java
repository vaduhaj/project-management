package com.PM;

import com.PM.Calendar.CalendarList;
import com.PM.ProjectStructure.ProjectDTO;
import com.PM.ProjectStructure.ProjectTask;
import com.PM.ProjectStructure.ProjectData;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.controlsfx.dialog.ExceptionDialog;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

public class Main extends Application {

    final private String resourceUrl = ProjectData.resourceUrl;

    private Stage primaryStage;
    private TreeTableView<ProjectTask> treeTable;
    private TreeTableProjectTask treeTableProjectTasks;
    private TreeChart treeChart;
    private ScrollPane scrollPane;
    private boolean chartVisibility;

    public static void main(String[] args) {
        Application.launch(args);
    }

    public void start(Stage stage) {

        this.treeTableProjectTasks = new TreeTableProjectTask(stage);
        VBox treeChart = treeTableProjectTasks.getTreeTableWithChart();

        Scene scene = new Scene(treeChart);

        primaryStage = stage;
        primaryStage.setScene(scene);
        primaryStage.setTitle("Project management");
        primaryStage.setResizable(true);
        primaryStage.setY(0);
        primaryStage.show();

    }

}

