package com.PM.Calendar;

import java.util.NoSuchElementException;

public enum CalendarSettings {
    EIGHTHOURWORKINGDAY,
    HOURSHIFT12,
    HOURSHIFT24,
    DAYSOFWEEKSETTINGS;

    private static final CalendarSettings[] ENUMS = values();

    CalendarSettings(){
    }

    public static CalendarSettings of(int calendarSettings) {
        if (calendarSettings >= 1 && calendarSettings <= 4) {
            return ENUMS[calendarSettings - 1];
        } else {
            throw new NoSuchElementException("Invalid value for DayOfWeek: " + calendarSettings);
        }
    }

    @Override
    public String toString() {

        if (this == EIGHTHOURWORKINGDAY) {
            return "8 hour working day";
        }else if (this == HOURSHIFT12) {
            return "12 hour shift";
        }else if (this == HOURSHIFT24) {
            return "24 hour shift";
        }else if (this == DAYSOFWEEKSETTINGS) {
            return "Days of week settings";
        }
        return "";

    }

}
