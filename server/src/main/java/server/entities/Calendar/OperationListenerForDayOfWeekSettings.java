package server.entities.Calendar;

import javax.persistence.PostLoad;
import java.time.DayOfWeek;

public class OperationListenerForDayOfWeekSettings {

    @PostLoad
    public void postLoad(DayOfWeekSettings dayOfWeekSettings) {

        dayOfWeekSettings.fillDayOfWeekString();

    }

}
