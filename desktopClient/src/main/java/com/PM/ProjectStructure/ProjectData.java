package com.PM.ProjectStructure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableView;
import lombok.Data;
import org.controlsfx.dialog.ExceptionDialog;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectData {

    public static final String resourceUrl = "http://localhost:8080/PM/%s";

    private LocalDateTime startDate;
    private LocalDateTime finishDate;
    private List<ProjectTasksHierarchy> hierarchy;
    private Map<Integer, Integer> changedTempIdsHierarchyElement;
    private List<ProjectTask> projectTasks;
    private List<ProjectTask> newProjectTasks;
    private Map<UUID, Map<String, Object>> changedProjectTasks;
    private List<UUID> deletedProjectTasks;

    private double duration;

    private boolean isCreatedByWbs;

    public ProjectData() {

        this.startDate = LocalDateTime.now();
        this.finishDate = startDate;
        this.hierarchy = new ArrayList<>();
        this.changedTempIdsHierarchyElement = new HashMap<>();
        this.projectTasks = new ArrayList<>();
        this.newProjectTasks = new ArrayList<>();
        this.changedProjectTasks = new HashMap<>();
        this.deletedProjectTasks = new ArrayList<>();
    }

    public ProjectData(ProjectDTO projectDTO) throws Exception {

        if (projectDTO.getException() != null) {
            throw projectDTO.getException();
        }

        this.startDate = LocalDateTime.now();
        this.finishDate = startDate;
        this.hierarchy = projectDTO.getHierarchy();
        this.projectTasks = projectDTO.getProjectTasks();
        this.newProjectTasks = projectDTO.getNewProjectTasks();
        this.deletedProjectTasks = projectDTO.getDeletedProjectTasks();
    }

    public void setupTestTree() {

        //this.isCreatedByWbs = true;

        int currentTempId = ProjectTask.getNewId();
        ProjectTask task1 = new ProjectTask(currentTempId++, ProjectTask.ROOT_ID, "Task 1",
                LocalDate.of(2021, 4, 19),
                LocalDate.of(2021, 6, 12));

        ProjectTask task11 = new ProjectTask(currentTempId++, task1.getTempId(), "Task 1.1",
                LocalDate.of(2021, 4, 19),
                LocalDate.of(2021, 6, 12));

        ProjectTask task12 = new ProjectTask(currentTempId++, task1.getTempId(), "Task 1.2",
                LocalDate.of(2021, 4, 19),
                LocalDate.of(2021, 6, 12));

        ProjectTask task13 = new ProjectTask(currentTempId++, task12.getTempId(), "Task 1.2.1",
                LocalDate.of(2021, 4, 19),
                LocalDate.of(2021, 6, 12));

        ProjectTask task2 = new ProjectTask(currentTempId++, ProjectTask.ROOT_ID, "Task 2",
                LocalDate.of(2021, 4, 19),
                LocalDate.of(2021, 6, 12));

        ProjectTask task21 = new ProjectTask(currentTempId++, task2.getTempId(), "Task 2.1",
                LocalDate.of(2021, 4, 19),
                LocalDate.of(2021, 6, 12));

        ProjectTask task22 = new ProjectTask(currentTempId, task2.getTempId(), "Task 2.2",
                LocalDate.of(2021, 4, 19),
                LocalDate.of(2021, 6, 12));


        projectTasks.add(task1);
        projectTasks.add(task11);
        projectTasks.add(task12);
        projectTasks.add(task13);
        projectTasks.add(task2);
        projectTasks.add(task21);
        projectTasks.add(task22);

        for (ProjectTask projectTask: projectTasks) {
            hierarchy.add(new ProjectTasksHierarchy(projectTask.getTempId(), projectTask.getTempParentId(),
                    null, null));
        }

    }

    public boolean isCreatedTreeByWbs() {
        return isCreatedByWbs;
    }

    public ProjectTask getRootTask() {
        ProjectTask rootTask = new ProjectTask("Root task",
                startDate.toLocalDate(),
                finishDate.toLocalDate());
        rootTask.setTempId(ProjectTask.ROOT_ID);
        return rootTask;
    }

    public void fillByTreeTableView(TreeTableView<ProjectTask> treeTableView) {

        TreeItem<ProjectTask> root = treeTableView.getRoot();
        if (root == null) {
            return;
        }
        ProjectTask rootProjectTask = root.getValue();
        //startDate = rootProjectTask.getStartDate();
        //finishDate = rootProjectTask.getFinishDate();
        duration = rootProjectTask.getDuration();
        projectTasks.clear();
        hierarchy.clear();
        newProjectTasks.clear();

        fillTreeRecursively(root.getChildren(), ProjectTask.defaultProjectTask);

    }

    public void setupTree(TreeTableView<ProjectTask> treeTable) {

        TreeItem<ProjectTask> rootTask = treeTable.getRoot();
        if (rootTask == null){
            rootTask = new TreeItem<>(this.getRootTask());
        }
        if (this.isCreatedTreeByWbs()){
            fillRootTaskByWbs(rootTask);
        }else{
            fillRootTaskById(rootTask);
        }

        treeTable.setRoot(rootTask);

    }

    public void addIdToChangedHierarchyElement(Integer id) {
        changedTempIdsHierarchyElement.putIfAbsent(id, id);
    }

    public void addChangedValueOfProjectTaskProperty(ProjectTask projectTask, String property, Object value) {

        UUID uuid = projectTask.getId();
        if (uuid == null) {
            return;
        }

        Map<String, Object> propertyValues = changedProjectTasks.get(uuid);

        if (propertyValues == null) {
            propertyValues = new HashMap<>();
        }

        propertyValues.put(property, value);

        changedProjectTasks.put(uuid, propertyValues);

    }

    public void setupNewTree(TreeTableView<ProjectTask> treeTable) {
        treeTable.setRoot(null);
        setupTree(treeTable);
    }

    public void setupTestDate(TreeTableView treeTable) {

        setupTestTree();
        setupTree(treeTable);
        getProjectTasks().clear();

    }

    private void fillRootTaskByWbs(TreeItem<ProjectTask> rootTask) {

        HashMap<Integer, TreeItem<ProjectTask>> parentsForEachLevel = new HashMap<>();
        parentsForEachLevel.put(1, rootTask);

        for (ProjectTask projectTask : projectTasks) {

            String wbs = projectTask.getWbs();
            int currentLevel = countOfOccurrences(wbs, ".");
            currentLevel++;

            TreeItem<ProjectTask> treeItem = new TreeItem<>(projectTask);
            TreeItem<ProjectTask> parent = parentsForEachLevel.get(currentLevel);
            parent.getChildren().add(treeItem);

            parentsForEachLevel.put(currentLevel + 1, treeItem);

        }

    }

    private void fillRootTaskById(TreeItem<ProjectTask> rootTask) {

        if (projectTasks == null || projectTasks.size() == 0) {
            return;
        }

        HashMap<Integer, TreeItem<ProjectTask>> idTreeItem = new HashMap<>();
        for (ProjectTask projectTask : projectTasks) {
            TreeItem<ProjectTask> treeItem = new TreeItem<>(projectTask);
            int id = projectTask.getTempId();
            idTreeItem.put(id, treeItem);
        }

        for (ProjectTasksHierarchy hierarchyElement : hierarchy) {
            int id = hierarchyElement.getTempId();

            TreeItem<ProjectTask> currentItem = idTreeItem.get(id);
            if (currentItem == null) {
                continue;
            }
            currentItem.getValue().setWbs(hierarchyElement.getWbs());
            int parentId = hierarchyElement.getTempParentId();
            TreeItem<ProjectTask> parentItem = idTreeItem.get(parentId);
            if (parentItem == null || parentId == ProjectTask.ROOT_ID){
                parentItem = rootTask;
            }

            if (parentItem == currentItem){
                continue;
            }

            parentItem.getChildren().add(currentItem);

        }

    }

    private int countOfOccurrences(String text, String subString) {

        int length = subString.length();
        if (length == 0){
            return 0;
        }

        int index = text.indexOf(subString);
        int count = 0;

        while(index >= 0) {
            index = text.indexOf(subString, index + length);
            count++;
        }

        return count;

    }

    private void fillTreeRecursively(ObservableList<TreeItem<ProjectTask>> treeItems, ProjectTask parent) {

        int count = treeItems.size();
        if (count == 0) {
            return;
        }
//        int digits = Integer.toString(count).length();
        int iterator = 1;
        for (TreeItem<ProjectTask> treeItem : treeItems) {
            ProjectTask projectTask = treeItem.getValue();

//            String numberWithLeadingZeros = getNumberWithLeadingZeros(digits, iterator++);
//            String wbs = parentWbs + numberWithLeadingZeros;
            if (changedTempIdsHierarchyElement.get(projectTask.getTempId()) != null || projectTask.getId() == null) {
                ProjectTasksHierarchy elem = new ProjectTasksHierarchy(projectTask.getTempId(), parent.getTempId(),
                        projectTask.getId(), parent.getId(), iterator);

                hierarchy.add(elem);
                if (projectTask.getId() == null) {
                    newProjectTasks.add(projectTask);
                }
            }
            iterator++;

            fillTreeRecursively(treeItem.getChildren(), projectTask);
        }

    }

//    private String getNumberWithLeadingZeros(int digits, int number) {
//        return String.format("%0" + digits + "d", number);
//    }

}
