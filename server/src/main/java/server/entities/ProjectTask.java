package server.entities;


import lombok.Data;
import server.entities.Calendar.Calendar;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Data
@Entity
@Table(name = "project_tasks")
public class ProjectTask implements Serializable {

    @Id
    @Column(name = "id")
    private UUID id;

    @Transient
    private int tempId;

    private String name = "";

    @Transient
    private String wbs;

    @Column(name = "start_date")
    private Date startDate;
    @Column(name = "finish_date")
    private Date finishDate;
    //private double duration; // Need BigDecimal

    @ManyToOne
    @JoinColumn(name = "calendar_id")
    private Calendar calendar;

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (!(o instanceof ProjectTask)) {
            return false;
        }

        ProjectTask that = (ProjectTask) o;

        return getId().equals(that.getId()) && getTempId() == that.getTempId();

    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tempId);
    }

    @Override
    public String toString() {
        return "Project tasks " +
                "'" + name + "' " +
                " wbs '" + wbs + "'";
    }

}
