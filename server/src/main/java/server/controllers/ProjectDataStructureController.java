package server.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import server.ProjectStructure.ProjectDTO;
import server.services.ProjectDataStructureService;

@RestController
public class ProjectDataStructureController {
    private ProjectDataStructureService projectDataStructureService;

    @Autowired
    public void setProjectTaskService(ProjectDataStructureService projectDataStructureService){
        this.projectDataStructureService = projectDataStructureService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ProjectData")
    public ProjectDTO findAllProjectTasks() {
        return projectDataStructureService.getProjectDTO();
    }

    @RequestMapping(method = RequestMethod.HEAD, value = "/ProjectData")
    public ProjectDTO header() {
        return new ProjectDTO();
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/ProjectData")
    @ResponseStatus(HttpStatus.OK)
    public ProjectDTO saveTreeProjectTasks(@RequestBody ProjectDTO projectDTO) {
        return projectDataStructureService.saveProjectDTO(projectDTO);
    }

}
