package server.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import server.entities.ProjectTask;

import java.util.List;
import java.util.UUID;

public interface ProjectTaskRepository extends JpaRepository<ProjectTask, UUID> {

    List<ProjectTask> findAll();

    List<ProjectTask> findAllById(Iterable ids);

    void deleteAllById(Iterable ids);
}
