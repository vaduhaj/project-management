package com.PM.ProjectStructure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectTasksHierarchy implements Serializable {

    public static final int ROOT_ID = 0;

    private UUID id;
    private UUID parentId;

    private int tempId;
    private int tempParentId;

    private String wbs;
    private int levelOrder;

    public ProjectTasksHierarchy(){
        
    }

    public ProjectTasksHierarchy(int tempId, int tempParentId, UUID id, UUID parentId){
        this.tempId = tempId;
        this.tempParentId = tempParentId;
        this.id = id;
        this.parentId = parentId;
    }

    public ProjectTasksHierarchy(int tempId, int tempParentId, UUID id, UUID parentId, int levelOrder){
        this(tempId, tempParentId, id, parentId);
        this.levelOrder = levelOrder;
    }

}
