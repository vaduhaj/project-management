package com.PM.ProjectStructure;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.*;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectTask implements Serializable, Cloneable {

    private static int currentId = 0;
    public static final int ROOT_ID = 0;
    public static final ProjectTask defaultProjectTask = setupNullProjectTask();

    private int tempId;
    private int tempParentId;
    private UUID id;
    private UUID parentId;
    private StringProperty name = new SimpleStringProperty();
    private StringProperty wbs = new SimpleStringProperty();
    private SimpleObjectProperty<Date> startDate = new SimpleObjectProperty<>();
    private SimpleObjectProperty<Date> finishDate = new SimpleObjectProperty<>();
    private double duration; // Need BigDecimal
//    private Calendar calendar;

    {
        tempId = currentId++;
    }

    public ProjectTask(){
        setupCurrentId();
    }

    public ProjectTask(String name, LocalDate startDate, LocalDate finishDate){
        this();
        this.name.set(name);// = name;
        this.startDate.setValue(Date.from(startDate.atStartOfDay().toInstant(ZoneOffset.ofHours(0)))); //= startDate;
        this.finishDate.setValue(Date.from(finishDate.atStartOfDay().toInstant(ZoneOffset.ofHours(0)))); //= finishDate;
        //this.duration = finishDateTime.compareTo(startDateTime);
    }

    public ProjectTask(LocalDate startDate){
        this("", startDate, startDate.plusDays(1L));
    }

    public ProjectTask(int tempId, int tempParentId, String name, LocalDate startDate, LocalDate finishDate){
        this(name, startDate, finishDate);
        this.tempId = tempId;
        this.tempParentId = tempParentId;
    }

    private void setupCurrentId() {
        if (currentId < this.tempId){
            currentId = this.tempId + 1;
        }
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getName() {
       return name.getValue();
    }

    public StringProperty getNameProperty() {
        return name;
    }

    public void setStartDate(Date startDate) {
        this.startDate.setValue(startDate);
    }

    public Date getStartDate() {
        return startDate.get();
    }

    public SimpleObjectProperty<Date> startDateProperty() {
        return startDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate.setValue(finishDate);
    }

    public SimpleObjectProperty<Date> finishDateProperty() {
        return finishDate;
    }

    public Date getFinishDate() {
        return finishDate.get();
    }

    public String getWbs() {
        return wbs.getValue();
    }

    public void setWbs(String wbs) {
        this.wbs.setValue(wbs);
    }

    public StringProperty getWbsProperty() {
        return wbs;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (!(o instanceof ProjectTask)) {
            return false;
        }

        ProjectTask that = (ProjectTask) o;

        return this.getTempId() == that.getTempId();

    }

    @Override
    public ProjectTask clone() throws CloneNotSupportedException {
        ProjectTask newProjectTask = (ProjectTask) super.clone();
        newProjectTask.setTempId(ProjectTask.getNewId());
        newProjectTask.setId(null);
        newProjectTask.setParentId(null);
        newProjectTask.name = new SimpleStringProperty(this.name.getValue());
        newProjectTask.wbs = new SimpleStringProperty();
        newProjectTask.startDate = new SimpleObjectProperty<>(this.startDate.getValue());
        newProjectTask.finishDate = new SimpleObjectProperty<>(this.finishDate.getValue());
        return newProjectTask;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getTempId(), this.getId());
    }

    @Override
    public String toString() {
        return "Project tasks " +
                "'" + name + "' " +
                " wbs '" + wbs + "'";
    }

    private static ProjectTask setupNullProjectTask(){
        ProjectTask nullProjectTask = new ProjectTask("", LocalDate.of(1, 1, 1), LocalDate.of(1, 1, 1));
        nullProjectTask.setTempId(ProjectTask.ROOT_ID);
        return nullProjectTask;
    }

    public static int getNewId() {
        return currentId++;
    }

}
