DROP FUNCTION IF EXISTS get_children_of_parent_in_depth(array_parent_id TEXT);
CREATE FUNCTION get_children_of_parent_in_depth(array_parent_id TEXT) RETURNS SETOF public.project_tasks_hierarchy AS
$BODY$
BEGIN
	RETURN QUERY
		WITH RECURSIVE hier AS (
		  SELECT
			ARRAY[project_tasks_hierarchy.id] id$
		  FROM
			project_tasks_hierarchy
		  WHERE
			project_tasks_hierarchy.id = ANY(array_parent_id::uuid[])
		UNION ALL
		  SELECT
			ARRAY(
			  SELECT
                project_tasks_hierarchy.id
			  FROM
				project_tasks_hierarchy
			  WHERE
				project_tasks_hierarchy.parent_id = ANY(hier.id$)
			) id$
		  FROM
			hier
		  WHERE
			COALESCE(id$, '{}') <> '{}' -- условие выхода из цикла - пустой массив
		),

		full_hierarchy_of_element AS (
        SELECT DISTINCT
			UNNEST(id$) id
		FROM
			hier)

        SELECT
            project_tasks_hierarchy.*
        FROM project_tasks_hierarchy
        JOIN full_hierarchy_of_element
            ON project_tasks_hierarchy.id = full_hierarchy_of_element.id;
END;
$BODY$

LANGUAGE plpgsql