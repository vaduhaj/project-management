DROP TABLE IF EXISTS "project_tasks";
DROP TABLE IF EXISTS "project_tasks_hierarchy";

CREATE TABLE project_tasks_hierarchy(
   id uuid NOT null,
   parent_id uuid REFERENCES project_tasks_hierarchy,
   level_order INT NOT null,
   CONSTRAINT project_tasks_hierarchy_pkey PRIMARY KEY (id)
);

CREATE INDEX ON project_tasks_hierarchy(parent_id);
CREATE INDEX ON project_tasks_hierarchy(wbs);

CREATE TABLE project_tasks(
   id uuid NOT null,
   name VARCHAR(150),
   start_date timestamp without time zone,
   finish_date timestamp without time zone,
   CONSTRAINT project_tasks_pkey PRIMARY KEY (id),
   CONSTRAINT fk_project_task_project_tasks_hierarchy_id
      FOREIGN KEY(id)
      REFERENCES project_tasks_hierarchy(id)
      ON DELETE CASCADE

);