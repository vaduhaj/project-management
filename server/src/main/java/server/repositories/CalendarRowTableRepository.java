package server.repositories;

import org.springframework.data.repository.Repository;
import server.entities.Calendar.Calendar;
import server.entities.Calendar.CalendarRowTable;

import java.util.List;
import java.util.UUID;

public interface CalendarRowTableRepository extends Repository<Calendar, UUID> {

    List<CalendarRowTable> findAll();

}
