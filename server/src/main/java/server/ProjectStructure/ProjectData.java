package server.ProjectStructure;

import lombok.Data;
import server.entities.ProjectTask;
import server.entities.ProjectTasksHierarchy;
import server.repositories.HierarchyRepository;
import server.repositories.ProjectTaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.lang.reflect.Field;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
public class ProjectData {

    private List<ProjectTasksHierarchy> hierarchy;
    private List<ProjectTask> projectTasks;
    private List<ProjectTask> newProjectTasks;
    private Map<UUID, Map<String, Object>> changedProjectTasks;
    private List<UUID> deletedProjectTasks;
    private Map<Integer, ProjectTasksHierarchy> mappedIdHierarchy;

    private HierarchyRepository hierarchyRepository;
    private ProjectTaskRepository projectTaskRepository;
    private EntityManagerFactory entityManagerFactory;

    public ProjectData(){

    }

    public ProjectData(ProjectDTO projectDTO){

        this.hierarchy = projectDTO.getHierarchy();
        this.projectTasks = projectDTO.getProjectTasks();
        this.newProjectTasks = projectDTO.getNewProjectTasks();
        this.changedProjectTasks = projectDTO.getChangedProjectTasks();
        this.deletedProjectTasks = projectDTO.getDeletedProjectTasks();

    }

    public void prepareDataForStorage(){

        prepareChangedHierarchy();
        prepareHierarchyElementsToDelete();
        prepareSavedNewProjectTasks();

    }

    public void saveData(){

        hierarchyRepository.saveAll(hierarchy);
        List<ProjectTasksHierarchy> allHierarchyElements = hierarchyRepository.findAllByOrderByLevelOrderAsc();
        List<ProjectTasksHierarchy> savedElements = getElementWithChangedLevelOrder(allHierarchyElements);
        hierarchyRepository.saveAll(savedElements);

        deleteHierarchyElements();
        projectTaskRepository.saveAll(newProjectTasks);
        saveChangedProjectTasks();

    }

    private void prepareChangedHierarchy(){

        if (hierarchy.size() == 0){
            return;
        }

        deleteNotExistedElementFromHierarchy();

        mappedIdHierarchy =
                hierarchy.stream()
                        .collect(Collectors.toMap(ProjectTasksHierarchy::getTempId, Function.identity()));

        hierarchy.stream()
                .filter(e -> e.getId() == null)
                .forEach(e -> {UUID newUUID = UUID.randomUUID();
                                e.setId(newUUID);}
        );

        for (ProjectTasksHierarchy hierarchyElement: hierarchy) {

            if (hierarchyElement.getParentId() != null) {
                continue;
            }

            int tempParentId = hierarchyElement.getTempParentId();
            ProjectTasksHierarchy parent = mappedIdHierarchy.get(tempParentId);
            UUID newUUID = null;
            if (parent != null) {
                newUUID = parent.getId();
            }

            hierarchyElement.setParentId(newUUID);

        }

    }

    private void deleteNotExistedElementFromHierarchy() {

        Stream<UUID> elementsUUID = hierarchy.stream()
                .map(ProjectTasksHierarchy::getId)
                .filter(Objects::nonNull);

        Stream<UUID> parentsUUID = hierarchy.stream()
                .map(ProjectTasksHierarchy::getParentId)
                .filter(Objects::nonNull);

        List<UUID> elementsToSearch = Stream.concat(elementsUUID, parentsUUID).distinct().collect(Collectors.toList());

        List<ProjectTasksHierarchy> existedElements = hierarchyRepository.findAllById(elementsToSearch);

        if (existedElements.size() == elementsToSearch.size()) {
            return;
        }

        Map<UUID, UUID> mappedId =
            existedElements.stream()
                .collect(Collectors.toMap(ProjectTasksHierarchy::getId, ProjectTasksHierarchy::getId));

        hierarchy = hierarchy.stream()
                .filter(p -> p.getId() == null || mappedId.get(p.getId()) != null)
                .collect(Collectors.toList());

        hierarchy.stream().filter(p -> mappedId.get(p.getParentId()) == null).forEach(p -> p.setParentId(null));

    }

    private void prepareSavedNewProjectTasks() {

        if (newProjectTasks.size() == 0) {
            return;
        }

        newProjectTasks = newProjectTasks.stream()
                .filter(p -> p.getId() == null && mappedIdHierarchy.get(p.getTempId()) != null)
                .collect(Collectors.toList());

        newProjectTasks.forEach(p -> {UUID newUUID = mappedIdHierarchy.get(p.getTempId()).getId();
                                      p.setId(newUUID);});

    }

    private void prepareHierarchyElementsToDelete(){

        deletedProjectTasks = deletedProjectTasks.stream().distinct().collect(Collectors.toList());

        if (deletedProjectTasks.size() == 0) {
            return;
        }

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        StoredProcedureQuery query = entityManager.createStoredProcedureQuery("get_children_of_parent_in_depth",
                ProjectTasksHierarchy.class);
        query.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
        String parameterValue = String.valueOf(deletedProjectTasks).replace('[', '{').replace(']', '}');
        query.setParameter(1, parameterValue);
        query.execute();
        List<ProjectTasksHierarchy> allHierarchyElements = (List<ProjectTasksHierarchy>) query.getResultList();
        TreeItem rootItem = TreeItem.getTreeById(allHierarchyElements);

        deletedProjectTasks.clear();
        fillDeletedProjectTasksRecursively(rootItem);

    }

    private void deleteHierarchyElements(){

        if (deletedProjectTasks.size() == 0) {
            return;
        }

        hierarchyRepository.deleteAllById(deletedProjectTasks);

    }

    private void fillDeletedProjectTasksRecursively(TreeItem treeItem){

        for (TreeItem currentTreeItem: treeItem.getChildren()) {
            fillDeletedProjectTasksRecursively(currentTreeItem);
            deletedProjectTasks.add(currentTreeItem.getValue().getId());
        }

    }

    private void saveChangedProjectTasks(){

        if (changedProjectTasks.isEmpty()) {
            return;
        }

        Set<UUID> idToFind = changedProjectTasks.keySet();
        List<ProjectTask> foundProjectTasks = projectTaskRepository.findAllById(idToFind);
        List<ProjectTask> savedProjectTasks = new ArrayList<>();
        foundProjectTasks.forEach(projectTask -> {
            UUID id = projectTask.getId();
            Map<String, Object> propertyValues = changedProjectTasks.get(id);
            boolean needToSave = false;

            for (Map.Entry<String, Object> kv: propertyValues.entrySet()) {
                String property = kv.getKey();
                Object value = kv.getValue();
                Field field;
                try {
                    field = ProjectTask.class.getDeclaredField(property);
                    field.setAccessible(true);
                    Object fieldValue = field.get(projectTask);
                    value = getValueByProperty(property, value);
                    if (fieldValue == null || !fieldValue.equals(value)) {
                        field.set(projectTask, value);
                        needToSave = true;
                    }
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

            if (needToSave) {
                savedProjectTasks.add(projectTask);
            }

        });

        if (savedProjectTasks.size() == 0) {
            return;
        }

        projectTaskRepository.saveAll(savedProjectTasks);

    }

    private Object getValueByProperty(String property, Object value) {

        Object calculatedValue = value;
        if ((property.equals("startDate") || property.equals("finishDate")) && value instanceof Long) {
            calculatedValue = Date.from(Instant.ofEpochMilli((Long) value));
        }

        return calculatedValue;

    }

    public void fillTempIds(){

        Map<UUID, ProjectTasksHierarchy> mappedIds = new HashMap<>();
        int tempId = 1;
        for (ProjectTasksHierarchy hierarchyElement: hierarchy) {
            hierarchyElement.setTempId(tempId++);
            mappedIds.put(hierarchyElement.getId(), hierarchyElement);
        }

        int tempParentId;
        for (ProjectTasksHierarchy hierarchyElement: hierarchy) {
            ProjectTasksHierarchy parent = mappedIds.get(hierarchyElement.getParentId());
            if (parent == null) {
                tempParentId = ProjectTasksHierarchy.ROOT_ID;
            } else {
                tempParentId = parent.getTempId();
            }
            hierarchyElement.setTempParentId(tempParentId);

        }

        for (ProjectTask projectTask: projectTasks) {
            ProjectTasksHierarchy hierarchyElement = mappedIds.get(projectTask.getId());
            if (hierarchyElement == null) {
                continue;
            }
            projectTask.setTempId(hierarchyElement.getTempId());
        }

    }

    private List<ProjectTasksHierarchy> getElementWithChangedLevelOrder(List<ProjectTasksHierarchy> allHierarchyElements){

        TreeItem rootItem = TreeItem.getTreeById(allHierarchyElements);
        List<TreeItem> children = rootItem.getChildren();
        List<ProjectTasksHierarchy> savedElements = new ArrayList<>();
        calculateLevelOrderRecursively(savedElements, children);

        return savedElements;

    }

    private void calculateLevelOrderRecursively(List<ProjectTasksHierarchy> savedElements, List<TreeItem> treeItems){

        int count = treeItems.size();
        if (count == 0) {
            return;
        }
//        int digits = Integer.toString(count).length();
        Integer iterator = 1;
        for (TreeItem treeItem: treeItems) {

//            String numberWithLeadingZeros = getNumberWithLeadingZeros(digits, iterator);
//            String wbs = parentWbs + numberWithLeadingZeros;
            ProjectTasksHierarchy currentItem = treeItem.getValue();
//            if (!wbs.equals(currentItem.getWbs())) {
//                currentItem.setWbs(wbs);
//                savedElements.add(currentItem);
//            }
            if (!(iterator).equals(currentItem.getLevelOrder())) {
                currentItem.setLevelOrder(iterator);
                savedElements.add(currentItem);
            }
            iterator++;
            List<TreeItem> treeItemsOfCurrentItem = treeItem.getChildren();
            calculateLevelOrderRecursively(savedElements, treeItemsOfCurrentItem);
        }

    }

//    private String getNumberWithLeadingZeros(int digits, int number) {
//        return String.format("%0" + digits + "d", number);
//    }

    private static class TreeItem {

        ProjectTasksHierarchy value;
        LinkedList<TreeItem> children = new LinkedList<>();

        public TreeItem(ProjectTasksHierarchy value) {
            this.value = value;
        }

        public ProjectTasksHierarchy getValue() {
            return value;
        }

        public LinkedList<TreeItem> getChildren() {
            return children;
        }

        public static TreeItem getTreeById(List<ProjectTasksHierarchy> hierarchyElements){

            Map<UUID, TreeItem> mappedIdAllElements = new HashMap<>();
            List<TreeItem> treeItems = new ArrayList<>(hierarchyElements.size());
            for (ProjectTasksHierarchy hierarchyElement: hierarchyElements) {
                TreeItem treeItem = new TreeItem(hierarchyElement);
                mappedIdAllElements.put(hierarchyElement.getId(), treeItem);
                treeItems.add(treeItem);
            }

            TreeItem rootItem = new TreeItem(new ProjectTasksHierarchy());
            for (TreeItem treeItem : treeItems) {
                ProjectTasksHierarchy projectTask = treeItem.getValue();
                UUID parentId = projectTask.getParentId();
                TreeItem parent = mappedIdAllElements.get(parentId);
                if (parent == null) {
                    parent = rootItem;
                }
                if (parent == treeItem){
                    continue;
                }
                parent.getChildren().add(treeItem);
            }

            return rootItem;

        }

    }

}
